/**
  ******************************************************************************
  * File Name          : stm32f4xx_can.c
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "stm32f4xx_can.h"

/** @brief  Handles for CAN1 and CAN2.
  */
CAN_HandleTypeDef hcan1;
CAN_HandleTypeDef hcan2;

/** @brief  Buffers for CAN1 and CAN2.
  */
CanRxMsgTypeDef CAN1_Rx_Mess;
CanTxMsgTypeDef CAN1_Tx_Mess;
CanRxMsgTypeDef CAN2_Rx_Mess;
CanTxMsgTypeDef CAN2_Tx_Mess;

/** @brief  Filter for both CAN1 and CAN2.
  */
CAN_FilterConfTypeDef CanFilter;

/** @brief  Prescaler for both CAN1 and CAN2.
  */
uint16_t CAN1_Prescaler = CAN_DEFAULT_PRESCALER;
uint16_t CAN2_Prescaler = CAN_DEFAULT_PRESCALER;

/**
  * @brief  This function initializes CAN filter.
  */
HAL_StatusTypeDef MX_CAN_DefaultFilter(CAN_HandleTypeDef* hcan){
	
	CanFilter.FilterMode = CAN_FILTERMODE_IDMASK;
	CanFilter.FilterScale = CAN_FILTERMODE_IDLIST;
	CanFilter.FilterIdHigh = 0x0000;
	CanFilter.FilterIdLow = 0x0000; 
	CanFilter.FilterMaskIdHigh = 0x0000;
	CanFilter.FilterMaskIdLow = 0x0000;
	CanFilter.FilterFIFOAssignment = CAN_FIFO0;
	CanFilter.FilterActivation = ENABLE;
	
	if(hcan->Instance == CAN1)
		CanFilter.FilterNumber = 0;	
	else if(hcan->Instance == CAN2)
		CanFilter.FilterNumber = 14;	
	
	return HAL_CAN_ConfigFilter(hcan, &CanFilter);
}

/**
  * @brief  This function updates CAN filter ID.
  */
HAL_StatusTypeDef MX_CAN_UpdateFilterId(CAN_HandleTypeDef* hcan, uint32_t v){

	CanFilter.FilterIdHigh = (v & 0xFFFF0000) > 32;
	CanFilter.FilterIdHigh = (v & 0x0000FFFF);
	
	if(hcan->Instance == CAN1)
		CanFilter.FilterNumber = 0;	
	else if(hcan->Instance == CAN2)
		CanFilter.FilterNumber = 14;
	
	return HAL_CAN_ConfigFilter(hcan, &CanFilter);
}

/**
  * @brief  This function updates CAN filter mask ID.
  */
HAL_StatusTypeDef MX_CAN_UpdateFilterMaskId(CAN_HandleTypeDef* hcan, uint32_t v){
	
	CanFilter.FilterMaskIdHigh = (v & 0xFFFF0000) > 32;
	CanFilter.FilterMaskIdLow = (v & 0x0000FFFF);
	
	if(hcan->Instance == CAN1)
		CanFilter.FilterNumber = 0;	
	else if(hcan->Instance == CAN2)
		CanFilter.FilterNumber = 14;
	
	return HAL_CAN_ConfigFilter(hcan, &CanFilter);
}

/**
  * @brief  This function initializes CAN1.
  */
HAL_StatusTypeDef MX_CAN1_Init(void){

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = CAN1_Prescaler;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SJW = CAN_SJW_1TQ;
  hcan1.Init.BS1 = CAN_BS1_14TQ;
  hcan1.Init.BS2 = CAN_BS2_6TQ;
  hcan1.Init.TTCM = DISABLE;
  hcan1.Init.ABOM = DISABLE;
  hcan1.Init.AWUM = DISABLE;
  hcan1.Init.NART = DISABLE;
  hcan1.Init.RFLM = DISABLE;
  hcan1.Init.TXFP = DISABLE;
  
	if(HAL_CAN_Init(&hcan1) != HAL_OK || MX_CAN_DefaultFilter(&hcan1) != HAL_OK)
		return HAL_ERROR;
	else
		return HAL_OK;
}

/**
  * @brief  This function initializes CAN2.
  */
HAL_StatusTypeDef MX_CAN2_Init(void){

  hcan2.Instance = CAN2;
  hcan2.Init.Prescaler = CAN2_Prescaler;
  hcan2.Init.Mode = CAN_MODE_NORMAL;
  hcan2.Init.SJW = CAN_SJW_1TQ;
  hcan2.Init.BS1 = CAN_BS1_14TQ;
  hcan2.Init.BS2 = CAN_BS2_6TQ;
  hcan2.Init.TTCM = DISABLE;
  hcan2.Init.ABOM = DISABLE;
  hcan2.Init.AWUM = DISABLE;
  hcan2.Init.NART = DISABLE;
  hcan2.Init.RFLM = DISABLE;
  hcan2.Init.TXFP = DISABLE;
  
	if(HAL_CAN_Init(&hcan2) != HAL_OK || MX_CAN_DefaultFilter(&hcan2) != HAL_OK)
		return HAL_ERROR;
	else
		return HAL_OK;
}

/**
  * @brief  This function initializes CAN MSP.
  */
void HAL_CAN_MspInit(CAN_HandleTypeDef* hcan){

  GPIO_InitTypeDef GPIO_InitStruct;
	
  if(hcan->Instance == CAN1){
		
    /* Peripheral clock enable */
    __CAN1_CLK_ENABLE();
  
    /**CAN1 GPIO Configuration    
    PD0     ------> CAN1_RX
    PD1     ------> CAN1_TX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* Peripheral interrupt init*/
    HAL_NVIC_SetPriority(CAN1_TX_IRQn, 0, 0);
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
  }
  else if(hcan->Instance == CAN2){

    /* Peripheral clock enable */
    __CAN1_CLK_ENABLE();
    __CAN2_CLK_ENABLE();
  
    /**CAN2 GPIO Configuration    
    PB5     ------> CAN2_RX
    PB6     ------> CAN2_TX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(CAN2_TX_IRQn, 0, 0);
    HAL_NVIC_SetPriority(CAN2_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN2_TX_IRQn);
    HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
  }
}

/**
  * @brief  This function deinitializes CAN MSP.
  */
void HAL_CAN_MspDeInit(CAN_HandleTypeDef* hcan){

  if(hcan->Instance == CAN1){
		
    /* Peripheral clock disable */
    __CAN1_CLK_DISABLE();
  
    /**CAN1 GPIO Configuration    
    PD0     ------> CAN1_RX
    PD1     ------> CAN1_TX 
    */
    HAL_GPIO_DeInit(GPIOD, GPIO_PIN_0|GPIO_PIN_1);

    /* Peripheral interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
  }
  else if(hcan->Instance == CAN2){
		
    /* Peripheral clock disable */
    __CAN2_CLK_DISABLE();
    __CAN1_CLK_DISABLE();
  
    /**CAN2 GPIO Configuration    
    PB5     ------> CAN2_RX
    PB6     ------> CAN2_TX 
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_5|GPIO_PIN_6);

    /* Peripheral interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN2_RX0_IRQn);
  }
}

/**
  * @brief  This function handles CAN1 RX0 interrupts.
  */
void CAN1_RX0_IRQHandler(void){
	
	/* Setting the blue LED */
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
	
	/* Signaling that the IRQ is being handled */
	HAL_CAN_IRQHandler(&hcan1);
  
	/* Handling the IRQ */
	CAN_Rx_Handler(CAN1_IF);
	
	/* Resetting the interrupt */
	while(HAL_CAN_Receive_IT(&hcan1, CAN_FIFO0) != HAL_OK);
}

/**
  * @brief  This function handles CAN2 RX0 interrupts.
  */
void CAN2_RX0_IRQHandler(void){
	
	/* Setting the blue LED */
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
	
	/* Signaling that the IRQ is being handled */
	HAL_CAN_IRQHandler(&hcan2);
	
	/* Handling the IRQ */
	CAN_Rx_Handler(CAN2_IF);
	
	/* Resetting the interrupt */
	while(HAL_CAN_Receive_IT(&hcan2, CAN_FIFO0) != HAL_OK);
}

/**
  * @brief  This function handles CAN1 TX interrupts.
  */
void CAN1_TX_IRQHandler(void)
{
  HAL_CAN_IRQHandler(&hcan1);
	CAN_Tx_Handler(CAN1_IF);
}

/**
  * @brief  This function handles CAN2 TX interrupts.
  */
void CAN2_TX_IRQHandler(void)
{
  HAL_CAN_IRQHandler(&hcan2);
	CAN_Tx_Handler(CAN2_IF);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
