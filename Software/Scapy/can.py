## This file is for use with Scapy
## See http://www.secdev.org/projects/scapy for more information
## Authors: Arnaud Lebrun, Jonathan-Christofer Demay
## This program is published under a GPLv2 license
from scapy.packet import *
from scapy.fields import *

class SocketCAN(Packet):
  name = 'SocketCAN'
  fields_desc = [
    BitEnumField('EFF', 0, 1, {0:'disabled', 1:'enabled'}),
    BitEnumField('RTR', 0, 1, {0:'disabled', 1:'enabled'}),
    BitEnumField('ERR', 0, 1, {0:'disabled', 1:'enabled'}),
    XBitField('id', 1, 29),
    FieldLenField('dlc', None, length_of='data', fmt='B'),
    ByteField('__pad', 0),
    ByteField('__res0', 0),
    ByteField('__res1', 0),
    StrLenField('data', '', length_from=lambda pkt: pkt.dlc)
  ]
  def extract_padding(self, p):
    return '',p
  def fragment(self):
    try:
      fragments = self.data.fragment()
    except:
      raise Exception('Fragmentation failed (perhaps upper layer is missing in the data field?)')
    lst = []
    fl = self
    while fl.underlayer is not None:
      fl = fl.underlayer
    for f in fragments:
      lst.append(fl.copy())
      lst[-1][SocketCAN].data = f
    return lst

class ISOTP(Packet):
  name = 'ISOTP'
  fields_desc = [
    BitEnumField('type', 0xf, 4, {0:'single', 1:'first', 2:'consecutive', 3:'flow_control'}),
    ConditionalField(BitField('pad', 0, 4), lambda pkt: pkt.type > 3),
    ConditionalField(BitField('size', 0, 4), lambda pkt: pkt.type == 0),
    ConditionalField(BitField('total_size', 0, 12), lambda pkt: pkt.type == 1),
    ConditionalField(BitField('index', 0, 4), lambda pkt: pkt.type == 2),
    ConditionalField(BitEnumField('flag', 0, 4, {0:'continue', 1:'wait', 2:'abort'}), lambda pkt: pkt.type == 3),
    ConditionalField(ByteField('block_size', 0), lambda pkt: pkt.type == 3),
    ConditionalField(ByteField('ST', 0), lambda pkt: pkt.type == 3),
    ConditionalField(StrLenField('data', '', length_from=lambda pkt: 6 if pkt.type == 1 else 7), lambda pkt: pkt.type < 3)
  ]
  def fragment(self):
    lst = []
    if self.type < 4:
      return lst.append(self)
    else:
      payload = str(self.payload)
      length = len(payload)
      if length <= 7:
        lst.append(ISOTP(type = 'single', size = length, data = str(payload)) / Padding(load = '\x00' * (7 - length)))
      else:
        lst.append(ISOTP(type = 'first', total_size = length, data = payload[:6]))
        payload = payload[6:]
        length = len(payload)
        payload = payload + ('\x00' * (0 if length % 7 == 0 else (7 - length % 7)))
        for i in range(length / 7 + (1 if length % 7 != 0 else 0)):
          lst.append(ISOTP(type = 'consecutive', index = i, data = payload[i * 7:(i + 1) * 7]))
    return lst
