/**
  ******************************************************************************
  * @file    canspy_flash.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Flash module core.
  *          This file provides all the functions to manage the flash:
  *           + Erasing
  *           + Programming
  *           + Overwriting
  *           + Assigning
  *           + Searching
  *           + Reading
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#include "canspy_flash.h"
#include "canspy_print.h"
#include "canspy_debug.h"

uint32_t canspy_flash_sector_addr[FLASH_SECTOR_TOTAL] = {

	0x08000000,
	0x08004000,
	0x08008000,
	0x0800C000,
	0x08010000,
	0x08020000,
	0x08040000,
	0x08060000,
	0x08080000,
	0x080A0000,
	0x080C0000,
	0x080E0000,
};

uint32_t canspy_flash_sector_size[FLASH_SECTOR_TOTAL] = {

	16 * 1024,
	16 * 1024,
	16 * 1024,
	16 * 1024,
	64 * 1024,
	128 * 1024,
	128 * 1024,
	128 * 1024,
	128 * 1024,
	128 * 1024,
	128 * 1024,
	128 * 1024,
};

/**
  * @brief  Get the sector of a flash address.
  * @param  flash_addr: the flash address to search.
  * @retval The corresponding flash sector or CANSPY_FLASH_LAST if not found
  */
uint32_t canspy_flash_sector(uint32_t flash_addr){

	int sector = CANSPY_FLASH_LAST;

	if(flash_addr >= FLASH_BASE || flash_addr <= FLASH_END){

		for(sector = CANSPY_FLASH_FIRST; sector < CANSPY_FLASH_LAST; sector++){

			if(flash_addr >= CANSPY_FLASH_HNDL(sector)
			&& flash_addr < CANSPY_FLASH_HNDL(sector) + CANSPY_FLASH_SIZE(sector)){

				break;
			}
		}
	}

	return sector;
}

/**
  * @brief  Erase a flash sector.
  * @param  sector: the flash sector to erase.
  * @retval Exit Status
  */
int canspy_flash_erase(uint32_t sector){

	HAL_StatusTypeDef result;
	uint32_t sector_error = 0;
	FLASH_EraseInitTypeDef erase_init = {

		.TypeErase = FLASH_TYPEERASE_SECTORS,
		.Sector = sector,
		.NbSectors = 1,
		.VoltageRange = FLASH_VOLTAGE_RANGE_3
	};

	if(sector >= CANSPY_FLASH_LAST){

		CANSPY_DEBUG_ERROR_0(ERROR, "Invalid sector");
		return EXIT_FAILURE;
	}

	HAL_FLASH_Unlock();
	CANSPY_FLASH_CLEAR_FLASG();
	result = HAL_FLASHEx_Erase(&erase_init, &sector_error);
	HAL_FLASH_Lock();

	if(result != HAL_OK){

		CANSPY_DEBUG_ERROR_2(ERROR, "Failed to erase sector %i: error %i", sector, result);
		return EXIT_FAILURE;
	}
	else
		return EXIT_SUCCESS;
}

/**
  * @brief  Test the validity of a flash writing operation.
  * @param  flash_addr: the flash address to test.
  * @param  data: a pointer to the data to write.
  * @param  size: the size of the data to write.
  * @param  sector_ptr: a pointer to return the sector or NULL.
  * @param  ignore: do not check for overwrite possibilities.
  * @retval CANSPY_FLASH_ERROR
  */
CANSPY_FLASH_ERROR canspy_flash_test(uint32_t flash_addr, void *data, int size, uint32_t *sector_ptr, bool ignore){

	int i;
	uint32_t sector;

	if(size <= 0)
		return FLASH_LEN;

	if((sector = canspy_flash_sector(flash_addr)) >= CANSPY_FLASH_LAST)
		return FLASH_ADDR;

	if(sector_ptr != NULL)
		*sector_ptr = sector;

	if(flash_addr + size > CANSPY_FLASH_HNDL(sector) + CANSPY_FLASH_SIZE(sector))
		return FLASH_BOUND;

	if(!ignore){

		for(i = 0; i < size; i++){

			if(((((uint8_t *)data)[i]) & (((uint8_t *)flash_addr)[i])) != ((uint8_t *)data)[i])
				return FLASH_SECTOR;
		}
	}

	return FLASH_OK;
}

/**
  * @brief  Write data to a flash address.
	* @param  flash_addr: the flash address to overwrite.
  * @param  data: a pointer to the data to write.
  * @param  size: the size of the data to write.
  * @retval Exit Status
  */
int canspy_flash_write(uint32_t flash_addr, void *data, int size){

	HAL_StatusTypeDef result;

	result = HAL_FLASH_Unlock();
	CANSPY_FLASH_CLEAR_FLASG();

	while(result == HAL_OK && (flash_addr % 4 && size >= 1)){

		result = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, flash_addr, *(uint8_t *)data);
		data = (uint8_t *)data + 1;
		flash_addr += 1;
		size -= 1;
	}

	while(result == HAL_OK && size >= 4){

		result = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, flash_addr, *(uint32_t *)data);
		data = (uint32_t *)data + 1;
		flash_addr += 4;
		size -= 4;
	}

	while(result == HAL_OK && size >= 1){

		result = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, flash_addr, *(uint8_t *)data);
		data = (uint8_t *)data + 1;
		flash_addr += 1;
		size -= 1;
	}

	HAL_FLASH_Lock();

	if(result != HAL_OK){

		CANSPY_DEBUG_ERROR_2(ERROR, "Failed to write flash at address 0x%08x: error %i", flash_addr, result);
		return EXIT_FAILURE;
	}
	else
		return EXIT_SUCCESS;
}

/**
  * @brief  Allocate a sector from the flash memory.
  * @param  size: the size of the requested allocation.
  * @param  flash_id: the identifier associated with the allocation.
  * @param  flash_print: a pointer to the printer function or NULL.
  * @param  realloc: specifies if it is a reallocation.
  * @retval The allocated flash address or NULL
  */
void *canspy_flash_alloc(size_t size, char flash_id[CANSPY_FLASH_IDSTR], CANSPY_FLASH_FUNC flash_print, bool realloc){

	int sector;

	if(*(uint8_t *)flash_id != 0xFF && !(realloc == false && canspy_flash_search(flash_id) != NULL)){

		for(sector = CANSPY_FLASH_FIRST; sector < CANSPY_FLASH_LAST; sector++){

			if(CANSPY_FLASH_FREE(sector) && size < CANSPY_FLASH_LEN(sector)){

				if(canspy_flash_write(CANSPY_FLASH_HNDL(sector), flash_id, CANSPY_FLASH_IDSTR) != EXIT_SUCCESS
				|| canspy_flash_write(CANSPY_FLASH_HNDL(sector), &flash_print, CANSPY_FLASH_PRINT) != EXIT_SUCCESS){

					CANSPY_DEBUG_ERROR_3(ERROR, "Failed to allocate flash sector %i with ID %s and %i bytes of data", sector, flash_id, size);
					canspy_flash_erase(sector);
				}
				else
					return CANSPY_FLASH_PTR(sector);
			}
		}
	}

	return NULL;
}

/**
  * @brief  Assign a flash sector to an identifier.
  * @param  size: the maximum size of the data to store.
  * @param  flash_id: the identifier to associate with the sector. 
  * @retval The address of the assigned flash sector or NULL
  */
void *canspy_flash_assign(size_t size, char flash_id[CANSPY_FLASH_IDSTR], CANSPY_FLASH_FUNC flash_print){

	return canspy_flash_alloc(size, flash_id, flash_print, false);
}

/**
  * @brief  Search the flash memory for an allocated sector.
  * @param  flash_id: the identifier associated with the allocation. 
  * @retval The allocated flash address or NULL
  */
void *canspy_flash_search(char flash_id[CANSPY_FLASH_IDSTR]){

	int sector;

	if(*(uint8_t *)flash_id != 0xFF){

		for(sector = CANSPY_FLASH_FIRST; sector < CANSPY_FLASH_LAST; sector++){

			if(!CANSPY_FLASH_FREE(sector)){

				if(*(uint32_t *)CANSPY_FLASH_IDS(sector) == *(uint32_t *)(flash_id))
					return CANSPY_FLASH_PTR(sector);
			}
		}
	}

	return NULL;
}

/**
  * @brief  Store data at a flash address.
	* @param  flash_ptr: a pointer to the flash address to overwrite.
  * @param  data: a pointer to the data to write.
  * @param  size: the size of the data to write.
  * @param  force: always erase sector before writing.
  * @retval Exit Status
  */
int canspy_flash_store(void **flash_ptr, void *data, int size, bool force){

	void *new_ptr;
	uint32_t sector;
	CANSPY_FLASH_ERROR result;

	result = canspy_flash_test((uint32_t)*flash_ptr, data, size, &sector, force);

	if(result == FLASH_LEN || result == FLASH_ADDR || data == NULL){

		CANSPY_DEBUG_ERROR_3(ERROR, "Invalid flash writing operation at 0x%08x with %i bytes from 0x%08x", *flash_ptr, size, data);
		return EXIT_FAILURE;
	}
	else{

		if(*flash_ptr != CANSPY_FLASH_PTR(sector)){

			CANSPY_DEBUG_ERROR_2(ERROR, "Failed to store data at 0x%08x: allocated flash address at 0x%08x", *flash_ptr, CANSPY_FLASH_PTR(sector));
			return EXIT_FAILURE;
		}
		else if(result == FLASH_BOUND || result == FLASH_SECTOR || force == true){

			if((new_ptr = canspy_flash_alloc(size, CANSPY_FLASH_IDS(sector), *CANSPY_FLASH_PRN(sector), true)) == NULL){

				CANSPY_DEBUG_ERROR_4(ERROR, "Failed to reallocate flash memory for ID %c%c%c%c", CANSPY_FLASH_IDS(sector)[0], CANSPY_FLASH_IDS(sector)[1], CANSPY_FLASH_IDS(sector)[2], CANSPY_FLASH_IDS(sector)[3]);
				return EXIT_FAILURE;
			}
			else{

				canspy_flash_erase(sector);
				*flash_ptr = new_ptr;
			}
		}
	}

	return canspy_flash_write((uint32_t)*flash_ptr, data, size);
}

/**
  * @brief  Buffered reading at a flash address.
	* @param  src: a pointer to the flash source address.
  * @param  dst: a pointer to the destination address.
  * @param  length: the number of bytes to read.
  * @param  p_read: a pointer to store the read bytes.
  * @retval Exit Status
  */
int canspy_flash_read(void *src, void *dst, int length, unsigned int *p_read){

	uint32_t sector;
	CANSPY_FLASH_ERROR result;

	result = canspy_flash_test((uint32_t)src, dst, length, &sector, true);

	if(result != FLASH_OK){

		if(result != FLASH_BOUND){

			CANSPY_DEBUG_ERROR_1(ERROR, "Invalid flash address %08x", src);
			return EXIT_FAILURE;
		}
		else
			length = (uint32_t)(CANSPY_FLASH_PTR(sector)) + CANSPY_FLASH_LEN(sector) - (uint32_t)src;
	}

	*p_read = length;

	if(memcpy(dst, src, length) != dst){

		CANSPY_DEBUG_ERROR_2(ERROR, "Invalid to copy data from address %08x to %08x", src, dst);
		return EXIT_FAILURE;
	}
	else
		return EXIT_SUCCESS;
}
