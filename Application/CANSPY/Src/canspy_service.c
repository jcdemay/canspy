/**
  ******************************************************************************
  * @file    canspy_service.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Sched module core.
  *          This file provides all the functions to manage services:
  *           + Registering
  *           + Starting
  *           + Stopping
  *           + Walking
  *           + Finding
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#include "canspy_service.h"
#include "canspy_device.h"
#include "canspy_sched.h"
#include "canspy_debug.h"

/**
  * @brief  Register a new service.
  * @param  dev_id: the device ID.
  * @param  svc_func: the processing function (MUST BE UNIQUE).
  * @param  svc_name: the name of the service (MUST BE UNIQUE).
  * @param  svc_desc: the description of the service.
  * @param  exclu: exclusive access to the device.
  * @param  async: asynchrone scheduling (prevents exclusive).
  * @param  start: the initial state of the service.
  * @retval Exit Status
  */
int canspy_service_register(CANSPY_DEVICE_ID dev_id, CANSPY_SERVICE_FUNC svc_func, const char *svc_name, const char *svc_desc, bool exclu, bool async, bool start){

	int len;
	CANSPY_SERVICE_DESC *svc_list;
	CANSPY_DEVICE_DESC *dev_ptr;
	CANSPY_OPTION_CALL opt_call;

	dev_ptr = CANSPY_DEVICE_POINTER(dev_id);
	svc_list = realloc(dev_ptr->svc_list, (dev_ptr->svc_length + 1) * sizeof(CANSPY_SERVICE_DESC));

	if(!svc_list){

		CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
		return EXIT_FAILURE;
	}

	opt_call.svc_ptr = &(svc_list[dev_ptr->svc_length]);

	svc_list[dev_ptr->svc_length].dev_ptr = dev_ptr;
	svc_list[dev_ptr->svc_length].svc_id = dev_ptr->svc_length;
	svc_list[dev_ptr->svc_length].svc_func = svc_func;
	svc_list[dev_ptr->svc_length].svc_opt = malloc(sizeof(CANSPY_OPTION_DESC));
	svc_list[dev_ptr->svc_length].svc_opt->var_list = NULL;
	svc_list[dev_ptr->svc_length].svc_opt->var_size = 0;
	svc_list[dev_ptr->svc_length].svc_name = svc_name;
	svc_list[dev_ptr->svc_length].svc_desc = svc_desc;
	svc_list[dev_ptr->svc_length].exclusive = exclu;
	svc_list[dev_ptr->svc_length].asynchrone = async;
	svc_list[dev_ptr->svc_length].started = false;
	dev_ptr->svc_list = svc_list;
	dev_ptr->svc_length++;

	if(async)
		svc_list[dev_ptr->svc_length].exclusive = false;

	if((len = strlen(svc_name)) > CANSPY_PRINT_MAX_Service)
		CANSPY_PRINT_MAX_Service = len;

	if(svc_func != NULL)
		svc_list[dev_ptr->svc_length - 1].svc_func(SVC_INIT, &opt_call);

	if(start){

		if(canspy_service_start(&(svc_list[dev_ptr->svc_length - 1]), CANSPY_NODEVICE, NULL, NULL) != EXIT_SUCCESS)
			CANSPY_DEBUG_ERROR_1(ERROR, "%s: failed to start service", svc_name);
	}

	CANSPY_DEBUG_SERVICE(&(svc_list[dev_ptr->svc_length - 1]));

	if(dev_id == CANSPY_UART && svc_func != NULL)
		svc_list[dev_ptr->svc_length - 1].svc_func(SVC_POST, &opt_call);

	return EXIT_SUCCESS;
}

/**
  * @brief  Start a registered service.
  * @param  svc_ptr: a pointer to the service structure (optional).
  * @param  dev_id: the id of the device (optional).
  * @param  svc_name: the name of the service (optional).
  * @param  svc_func: the address of the service (optional).
  * @retval Exit Status
  */
int canspy_service_start(CANSPY_SERVICE_DESC *svc_ptr, CANSPY_DEVICE_ID dev_id, const char *svc_name, CANSPY_SERVICE_FUNC svc_func){

	if(svc_ptr == NULL)
		svc_ptr = canspy_service_find(dev_id, svc_name, svc_func);

	if(svc_ptr != NULL && CANSPY_DEVICE_ACTIVE(svc_ptr->dev_ptr->dev_id) && svc_ptr->started == false){

		if(svc_ptr->svc_func(SVC_START, NULL) != EXIT_SUCCESS){

			svc_ptr->svc_func(SVC_STOP, NULL);
			return EXIT_FAILURE;
		}

		svc_ptr->started = true;

		if(svc_ptr->asynchrone)
			CANSPY_DEVICE_POINTER(svc_ptr->dev_ptr->dev_id)->svc_async++;
		else
			CANSPY_DEVICE_POINTER(svc_ptr->dev_ptr->dev_id)->svc_sync++;

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}

/**
  * @brief  Stop a registered service.
  * @param  svc_ptr: a pointer to the service structure (optional).
  * @param  dev_id: the id of the device (optional).
  * @param  svc_name: the name of the service (optional).
  * @param  svc_func: the address of the service (optional).
  * @retval Exit Status
  */
int canspy_service_stop(CANSPY_SERVICE_DESC *svc_ptr, CANSPY_DEVICE_ID dev_id, const char *svc_name, CANSPY_SERVICE_FUNC svc_func){

	if(svc_ptr == NULL)
		svc_ptr = canspy_service_find(dev_id, svc_name, svc_func);

	if(svc_ptr != NULL && CANSPY_DEVICE_ACTIVE(svc_ptr->dev_ptr->dev_id) && svc_ptr->started == true){

		if(svc_ptr->asynchrone)
			CANSPY_DEVICE_POINTER(svc_ptr->dev_ptr->dev_id)->svc_async--;
		else
			CANSPY_DEVICE_POINTER(svc_ptr->dev_ptr->dev_id)->svc_sync--;

		svc_ptr->started = false;
		svc_ptr->svc_func(SVC_STOP, NULL);

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}

/**
  * @brief  Walk through all services of a specified device.
  * @param  dev_ptr: the specified device.
  * @param  cur_svc: the current service or NULL to start.
  * @retval The next service
  */
CANSPY_SERVICE_DESC *canspy_service_walk(CANSPY_DEVICE_DESC *dev_ptr, CANSPY_SERVICE_DESC *cur_svc){

	if(dev_ptr == NULL || dev_ptr->svc_length <= 0)
		return NULL;

	if(cur_svc == NULL)
		return &(dev_ptr->svc_list[0]);

	if(cur_svc->svc_id >= dev_ptr->svc_length - 1)
		return NULL;

	return &(dev_ptr->svc_list[cur_svc->svc_id + 1]);
}

/**
  * @brief  Search for a registered service.
  * @param  dev_id: the id of the device (optional).
  * @param  svc_name: the name of the service (optional).
  * @param  svc_func: the address of the service (optional).
  * @retval A pointer to the service structure
  */
CANSPY_SERVICE_DESC *canspy_service_find(CANSPY_DEVICE_ID dev_id, const char *svc_name, CANSPY_SERVICE_FUNC svc_func){

	uint16_t s, e;
	uint16_t i, j;

	if(svc_name == NULL && svc_func == NULL && dev_id == CANSPY_NODEVICE){

		return NULL;
	}

	/* If no device is specified then all devices are considered */
	if(dev_id == CANSPY_NODEVICE){

		s = CANSPY_DEVICE_FIRST;
		e = CANSPY_DEVICES;
	}
	else{

		s = dev_id;
		e = s + 1;
	}

	for(i = s; i < e; i++){

		for(j = 0; j < CANSPY_DEVICE_POINTER(i)->svc_length; j++){

			/* If no service is specified either by name or address, we return the first activated exclusive service */
			if((svc_name == NULL && svc_func == NULL && CANSPY_DEVICE_POINTER(i)->svc_list[j].started && CANSPY_DEVICE_POINTER(i)->svc_list[j].exclusive)
			|| (svc_name != NULL && strncmp(svc_name, CANSPY_DEVICE_POINTER(i)->svc_list[j].svc_name, CANSPY_PRINT_MAX_Service) == 0)
			|| (svc_func != NULL && CANSPY_DEVICE_POINTER(i)->svc_list[j].svc_func == svc_func)){

				return &(CANSPY_DEVICE_POINTER(i)->svc_list[j]);
			}
		}
	}

	return NULL;
}
