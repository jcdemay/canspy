/**
  ******************************************************************************
  * File Name          : stm32f4xx_eth.h
  * Description        : This file provides code for the configuration
  *                      of the ETH instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F4xx_ETH_H
#define __STM32F4xx_ETH_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Exported globals --------------------------------------------------------- */
extern ETH_HandleTypeDef heth;
extern uint8_t ETH_Rx_Buf[ETH_RXBUFNB][ETH_MAX_PACKET_SIZE];
extern uint8_t ETH_Tx_Buf[ETH_TXBUFNB][ETH_MAX_PACKET_SIZE];
extern ETH_DMADescTypeDef ETH_DMA_Rx_Desc[ETH_RXBUFNB];
extern ETH_DMADescTypeDef ETH_DMA_Tx_Desc[ETH_TXBUFNB];
extern uint8_t ETH_Mac_Src[6];
extern uint8_t ETH_Mac_Dst[6];

/* Exported functions ------------------------------------------------------- */
int ETH_Rx_Handler(uint8_t SegCount);
HAL_StatusTypeDef MX_ETH_Init(void);

#ifdef __cplusplus
}
#endif
#endif /* __STM32F4xx_ETH_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
